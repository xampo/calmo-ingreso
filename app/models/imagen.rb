class Imagen < ActiveRecord::Base

	belongs_to :user
	##Configuracion de la imagen de perfil
	##
	has_attached_file :imagen, :styles => { :medium => "200x200>", :thumb => "100x100>" },
	                :default_url => "/assets/stores/default/:style/missing.png",
	                :url  => "/assets/stores/:id/:style/:basename.:extension",
	                :path => ":rails_root/public/assets/stores/:id/:style/:basename.:extension"
	 
	validates_attachment_content_type :imagen, :content_type => /\Aimage\/.*\Z/

	validates_with AttachmentSizeValidator, :attributes => :imagen, :less_than => 2.megabytes

end
