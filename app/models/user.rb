class User < ActiveRecord::Base
	


	has_many :socials
	has_many :imagens

	
	## agregar nombre/id redes sociales
	accepts_nested_attributes_for :socials,:reject_if => lambda { |a| a[:nombre].blank? },  allow_destroy: true
	accepts_nested_attributes_for :imagens,:reject_if => lambda { |a| a[:imagen].blank? },  allow_destroy: true

	def self.searchRut(term)
		
  		where('LOWER(rut) LIKE :term', term: "%#{term.downcase}%")
	end
	def self.apellido(term)
  		where('LOWER(apellido) LIKE :term', term: "%#{term.downcase}%")
	end
end
