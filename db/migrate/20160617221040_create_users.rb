class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :rut
      t.string :nombre
      t.string :apellido
      t.string :email
      t.string :fec_nac
      t.string :password

      t.timestamps null: false
    end
  end
end
