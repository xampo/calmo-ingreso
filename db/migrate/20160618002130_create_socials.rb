class CreateSocials < ActiveRecord::Migration
  def change
    create_table :socials do |t|
      t.string :nombre
      t.belongs_to :user, index:true
      t.timestamps null: false
    end
  end
end
