class CreateImagens < ActiveRecord::Migration
  def change
    create_table :imagens do |t|
      t.string :imagen_file_name
      t.string :imagen_content_type
      t.integer :imagen_file_size
      t.datetime :imagen_updated_at
      t.belongs_to :user, index:true
      t.timestamps null: false
    end
  end
end
