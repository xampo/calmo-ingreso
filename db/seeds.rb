# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
usuarios = User.create([{ rut: '12334456-9',nombre: 'Pedro',apellido:'Perez', email: 'hola@perez.com' }, 
							{ rut: '77777777-9',nombre: 'Juan',apellido:'Rojas', email: 'hola1@perez.com' }, 
							{ rut: '11111111-9',nombre: 'Ignacio',apellido:'Herrera', email: 'hola2@perez.com' }, 
							{ rut: '33333333-9',nombre: 'Francisco',apellido:'Pereira', email: 'hola3@perez.com' }, 
							{ rut: '44444444-9',nombre: 'Felipe',apellido:'Maturana', email: 'hola4@perez.com' }, 
							{ rut: '55555555-9',nombre: 'Esteban',apellido:'Stern', email: 'hola5@perez.com' }, 

	])